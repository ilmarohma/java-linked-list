public class Stack{
	ListNode tos = new ListNode();

	public void printList(){
		ListNode tmp = new ListNode();
		tmp = tos;
		if(tmp.next == null){
			System.out.print("EMPTY LIST");
		}
		while(tmp.next != null){
			System.out.print(tmp.next.element);
			System.out.print(" ");
			tmp = tmp.next;
		}
		System.out.println();
	}

	public void push(Object element){
		ListNode newNode = new ListNode(element);
		newNode.next = tos.next;
		tos.next = newNode;
	}

	public void pop(){
		tos = tos.next;
	}

	public Object top(){
		return(tos.next.element);
	}
	public Object topAndPop(){
		Object ret = tos.next.element;
		this.pop();
		return ret;
	}

	public boolean isEmpty(){
		return(tos.next==null);
	}

	public void makeEmpty(){
		tos.next = null;
	}

	public int size(){
		int count = 0;
		ListNode tmp = new ListNode();
		tmp = tos;
		while(tmp.next != null){
			count++;
			tmp = tmp.next;
		}
		return count;
	}
}