public class Queue{
	DNode front = new DNode();
	DNode back = new DNode();

	public void enqueue(Object element){
		DNode newNode = new DNode(element);
		if(front.next == null){
			front.next = back.prev = newNode;
		}
		newNode.next = front.next;
		front.next.prev = newNode;
		front.next = newNode;
	}

	public Object dequeue(){
		if(front.next == null){
			return null;
		}
		DNode temp = back.prev;
		back.prev = back.prev.prev;
		return temp.element;
	}
}