
public class ListNode{
    Object element;
    ListNode next;

    public ListNode(Object theElement, ListNode n){
        element = theElement;
        next = n;
    }

    public ListNode(Object theElement){
        this(theElement, null);
    }
    public ListNode(){
        this(null, null);
    }
}