
public class LinkedList{
	ListNode head = new ListNode();

	public void printList(){
		ListNode tmp = new ListNode();
		tmp = head;
		if(tmp.next == null){
			System.out.print("EMPTY LIST");
		}
		while(tmp.next != null){
			System.out.print(tmp.next.element);
			System.out.print(" ");
			tmp = tmp.next;
		}
		System.out.println();
	}

	//nambah elemen di belakang
	public void insert(Object element){
		ListNode newNode = new ListNode(element);
		ListNode tmp = new ListNode();
		tmp = head;
		if(tmp == null){
			tmp=newNode;
		}
		while(tmp.next != null){
			tmp = tmp.next;
		}
		tmp.next = newNode;
	}

	//masukin elemen setelah suatu key (ditengah tengah gitu)
	public void inAfter(Object element, Object key1){
		ListNode newNode = new ListNode(element);
		ListNode lstkey1 = new ListNode();
		ListNode tmp = new ListNode();
		tmp = head;
		while(tmp.next != null){
			if(tmp.element == key1){
				lstkey1 = tmp;
			}
			tmp=tmp.next;
		}
		newNode.next = lstkey1.next;
		lstkey1.next = newNode;
	}

	//nambahin diawal
	public void addFirst(Object element){
		ListNode newNode = new ListNode(element);
		newNode.next = head.next;
		head.next = newNode;

	}

	//menghapus yang dibelakang
	public void delete(Object element){
		ListNode tmp = new ListNode();
		tmp = head;
		ListNode prev = new ListNode();
		if(tmp.element == element){
			head = tmp.next;
		}
		else{
			while(tmp.next != null && tmp.element != element){
				prev = tmp;
				tmp = tmp.next;
			}
			if(tmp != null){
				prev.next = tmp.next;
			}
		}
	}

	//kosong ga
	public boolean isEmpty(){
		return(head.next==null);
	}
	
	//ngosongin linkedlist	
	public void makeEmpty(){
		head.next = null;
	}

	//ukuran linkedlist-nya
	public int size(){
		int count = 0;
		ListNode tmp = new ListNode();
		tmp = head;
		while(tmp.next != null){
			count++;
			tmp = tmp.next;
		}
		return count;
	}

}