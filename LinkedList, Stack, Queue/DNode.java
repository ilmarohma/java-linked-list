
public class DNode{
    Object element;
    DNode next;
    DNode prev;

    public DNode(Object theElement, DNode n, DNode m){
        element = theElement;
        next = n;
        prev = m;
    }

    public DNode(Object theElement){
        this(theElement, null, null);
    }
    public DNode(){
        this(null, null, null);
    }
}

//sssssssssssssssss