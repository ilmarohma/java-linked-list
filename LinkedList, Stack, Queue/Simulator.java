public class Simulator{
	public static void main(String[] args) {
		LinkedList lst = new LinkedList();
		LinkedList lst1 = new LinkedList();

		lst.insert(5);
		lst.insert(6);
		lst.insert(7);
		lst.inAfter(8, 6);
		lst.printList();
		//lst.delete(8);
		lst.printList();
		System.out.println(lst.isEmpty());
		System.out.println(lst1.isEmpty());
		//lst.makeEmpty();
		lst.printList();
		System.out.println(lst.size());
		lst.addFirst(4);
		lst.printList();
		lst1.addFirst(3);
		lst1.printList();
		System.out.println(lst1.size());

		System.out.println("STACK");
		Stack stc = new Stack();
		Stack stc1 = new Stack();
		stc.push(5);
		stc.push(6);
		stc.printList();
		stc.pop();
		stc.push(7);
		stc.push(8);
		stc.printList();
		System.out.println(stc.top());
		stc.printList();
		System.out.println(stc.topAndPop());
		stc.printList();
		System.out.println(stc.size());
		System.out.println(stc1.size());
		System.out.println(stc.isEmpty());
		System.out.println(stc1.isEmpty());
		//stc.makeEmpty();
		stc.printList();


		System.out.println("QUEUE");
		Queue q = new Queue();
		q.enqueue(3);
		q.enqueue(4);
		q.enqueue(5);
		q.enqueue(1);
		q.dequeue();
		System.out.println(q.dequeue());



	}
}