public class Simulator{
	public static void main(String[] args) {
		BinaryTree tree = new BinaryTree();
		tree.root = new Node(1); 
        tree.root.left = new Node(2); 
        tree.root.right = new Node(3); 
        tree.root.left.left = new Node(4); 
        tree.root.left.right = new Node(5);

        System.out.println(tree.height());

        System.out.println("PRE-ORDER");
        tree.preOrder();
        System.out.println("\nPOST-ORDER");
        tree.postOrder();
        System.out.println("\nIN-ORDER");
        tree.inOrder();
        System.out.println("\nTERBESAR");
        System.out.println(tree.terbesar());
        System.out.println("\nJUMLAH");
        System.out.println(tree.sum());

        System.out.println();
        System.out.println();

        BinaryTree tree1 = new BinaryTree();
		tree1.root = new Node(80); 
        tree1.root.left = new Node(90); 
        tree1.root.left.left = new Node(40); 
        tree1.root.left.right = new Node(55);
        tree1.root.right = new Node(75); 
        tree1.root.right.left = new Node(81);
        tree1.root.right.right = new Node(30);
        tree1.root.right.left.right = new Node(71);
        tree1.root.right.left.left  = new Node(70);
        tree1.root.right.left.left.right  = new Node(10);

        System.out.println(tree1.height());

        System.out.println("PRE-ORDER");
        tree1.preOrder();
        System.out.println("\nPOST-ORDER");
        tree1.postOrder();
        System.out.println("\nIN-ORDER");
        tree1.inOrder();
        System.out.println("\nTERBESAR");
        System.out.println(tree1.terbesar());
        System.out.println("\nJUMLAH");
        System.out.println(tree1.sum());



	}
}