
public class BinaryTree{
	Node root;

	public BinaryTree(){
		root = null;
	}

	public int height (Node t){
		if (t == null) {
			return -1;
		} 
		else {
			return max(height (t.left) + 1, height (t.right) + 1);
		}
	}

	public int height (){
		return this.height(root);
	}

	public int max(int a, int b){
		if (a >= b){
			return a;
		}
		else{
			return b;
		}
	}

	public void preOrder(Node root){
		if(root == null){
			return;
		}
		System.out.print(root.element + " ");
		if(root.left != null){
			preOrder(root.left);
		}
		if(root.right != null){
			preOrder(root.right);
		}
	}

	public void preOrder(){
		this.preOrder(root);
	}

	public void postOrder(Node root){
		if(root == null){
			return;
		}
		if(root.left != null){
			postOrder(root.left);
		}
		if(root.right != null){
			postOrder(root.right);
		}
		System.out.print(root.element + " ");
	}

	public void postOrder(){
		this.postOrder(root);
	}

	public void inOrder(Node root){
		if(root == null){
			return;
		}
		if(root.left != null){
			inOrder(root.left);
		}

		System.out.print(root.element + " ");

		if(root.right != null){
			inOrder(root.right);
		}
	}

	public void inOrder(){
		this.inOrder(root);
	}

	int besar = 0;
	public void terbesar(Node root){
		if(root == null){
			return;
		}
		if(root.element > besar){
			besar = root.element;
		}
		if(root.left != null){
			terbesar(root.left);
		}
		if(root.right != null){
			terbesar(root.right);
		}
	}

	public int terbesar(){
		terbesar(root);
		return besar;
	}

	int jumlah = 0;
	public void sum(Node root){
		if(root == null){
			return;
		}
		jumlah += root.element;
		if(root.left != null){
			sum(root.left);
		}
		if(root.right != null){
			sum(root.right);
		}
	}

	public int sum(){
		sum(root);
		return jumlah;
	}

}