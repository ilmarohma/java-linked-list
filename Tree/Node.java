public class Node 
{ 
    int element; 
    Node left, right; 
  
    public Node(int element) 
    { 
        this.element = element; 
        this.left = this.right = null; 
    } 
} 
  